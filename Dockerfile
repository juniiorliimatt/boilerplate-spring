FROM openjdk:11.0.13-slim-buster
VOLUME /tmp
COPY target/gead-api-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]
