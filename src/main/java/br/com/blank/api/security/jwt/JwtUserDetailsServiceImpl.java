package br.com.blank.api.security.jwt;

import br.com.blank.api.domain.seguranca.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsServiceImpl implements UserDetailsService{

  private final UsuarioService userService;

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException{

	User user = userService.findByEmail(email);

	if(user == null){
	  throw new UsernameNotFoundException(String.format("No user found with username '%s'", email));
	}else{
	  return JwtUserFactory.create(user);
	}
  }

}
