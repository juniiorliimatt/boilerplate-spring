package br.com.blank.api.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenUtil implements Serializable{

  private static final long serialVersionUID = -6657474208632018376L;

  static final String CLAIM_KEY_USERNAME = "sub";
  static final String CLAIM_KEY_CREATED = "created";
  static final String CLAIM_KEY_EXPIRED = "exp";

  @Value("${API_SECRET}")
  private String secret;

  @Value("${API_EXPIRATIONS_MS}")
  private Long expiration;

  public String getUsernameFromToken(String token){
	String username = null;

	try{
	  final Claims claims = getClaimsFromToken(token);
	  if(claims != null){
		username = claims.getSubject();
	  }
	}catch(Exception e){
	  e.printStackTrace();
	}

	return username;
  }

  public Date getExpirationDateFromToken(String token){
	Date expirationDate = null;
	try{
	  final Claims claims = getClaimsFromToken(token);
	  if(claims != null){
		expirationDate = claims.getExpiration();
	  }
	}catch(Exception e){
	  e.printStackTrace();
	}

	return expirationDate;
  }

  private Claims getClaimsFromToken(String token){
	Claims claims;

	try{
	  claims = Jwts.parser()
				   .setSigningKey(secret)
				   .parseClaimsJws(token)
				   .getBody();
	}catch(Exception e){
	  claims = null;
	}

	return claims;
  }

  private Boolean isTokenExpired(String token){
	final Date expirationDate = getExpirationDateFromToken(token);
	return expirationDate.before(new Date());
  }

  public String generateToken(UserDetails userDetails){
	Map<String, Object> claims = new HashMap<>();

	claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());

	final Date createdDate = new Date();
	claims.put(CLAIM_KEY_CREATED, createdDate);

	return doGenerateToken(claims);
  }

  private String doGenerateToken(Map<String, Object> claims){

	final Date createdDate = (Date) claims.get(CLAIM_KEY_CREATED);
	final Date expirationDate = new Date(createdDate.getTime() + expiration * 1000);

	return Jwts.builder()
			   .setClaims(claims)
			   .setExpiration(expirationDate)
			   .signWith(SignatureAlgorithm.HS512, secret)
			   .compact();

  }

  public Boolean canTokenBeRefreshed(String token){
	return (!isTokenExpired(token));
  }

  public String refreshToken(String token){

	String refreshedToken;

	try{
	  final Claims claims = getClaimsFromToken(token);
	  if(claims != null){
		claims.put(CLAIM_KEY_CREATED, new Date());
	  }
	  refreshedToken = doGenerateToken(claims);
	}catch(Exception e){
	  refreshedToken = null;
	}

	return refreshedToken;
  }

  public Boolean validateToken(String token, UserDetails userDetails){
	JwtUser user = (JwtUser) userDetails;
	final String username = getUsernameFromToken(token);

	return (username.equals(user.getUsername()) && !isTokenExpired(token));

  }

}
