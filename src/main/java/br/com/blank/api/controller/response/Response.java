package br.com.blank.api.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.springframework.http.HttpStatus;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 30/01/2022
 */

@Getter
@SuperBuilder
@JsonInclude(NON_NULL)
public class Response{

  private String timeStamp;
  private int statusCode;
  private HttpStatus status;
  private String reason;
  private String message;
  private String developMessage;
  private List<?> data;

}
