package br.com.blank.api.controller.dto;

import br.com.blank.api.domain.seguranca.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 30/01/2022
 */

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class CurrentUserDto{
  private String token;
  private User user;

  public static CurrentUserDto convertToDto(CurrentUserDto currentUser){
	ModelMapper mapper = new ModelMapper();
	return mapper.map(currentUser, CurrentUserDto.class);
  }
}
