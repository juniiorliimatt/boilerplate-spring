package br.com.blank.api.controller.seguranca;

import br.com.blank.api.controller.dto.CurrentUserDto;
import br.com.blank.api.controller.response.Response;
import br.com.blank.api.domain.seguranca.User;
import br.com.blank.api.security.jwt.JwtAuthenticationRequest;
import br.com.blank.api.security.jwt.JwtTokenUtil;
import br.com.blank.api.utils.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Slf4j
public class AuthenticationRestController{

  private final AuthenticationManager authenticationManager;
  private final JwtTokenUtil jwtTokenUtil;
  private final UserDetailsService userDetailsService;
  private final UsuarioService userService;

  @PostMapping(value = "/api/auth")
  public ResponseEntity<Response> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest){

	final Authentication authentication = authenticationManager.authenticate(
		new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), authenticationRequest.getPassword()));

	SecurityContextHolder.getContext()
						 .setAuthentication(authentication);

	log.info(authenticationRequest.getEmail());

	final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
	final String token = jwtTokenUtil.generateToken(userDetails);
	final User user = userService.findByEmail(authenticationRequest.getEmail());
	user.setPassword(null);
	return ResponseEntity.ok()
						 .body(Response.builder()
									   .timeStamp(Utils.formatDate())
									   .data(List.of(CurrentUserDto.convertToDto(new CurrentUser(token, user))))
									   .message("Current User")
									   .status(OK)
									   .statusCode(OK.value())
									   .build());

  }

  @PostMapping(value = "/api/refresh")
  public ResponseEntity<Response> refreshAndGetAuthenticationToken(HttpServletRequest request){

	String token = request.getHeader("Authorization");
	String username = jwtTokenUtil.getUsernameFromToken(token);
	final User user = userService.findByEmail(username);
	boolean canTokenBeRefreshed = jwtTokenUtil.canTokenBeRefreshed(token);
	if(canTokenBeRefreshed){
	  String refreshToken = jwtTokenUtil.refreshToken(token);
	  return ResponseEntity.ok()
						   .body(Response.builder()
										 .timeStamp(Utils.formatDate())
										 .data(
											 List.of(CurrentUserDto.convertToDto(new CurrentUser(refreshToken, user))))
										 .message("Current User")
										 .status(OK)
										 .statusCode(OK.value())
										 .build());
	}else{
	  return ResponseEntity.badRequest()
						   .body(null);
	}
  }

}
