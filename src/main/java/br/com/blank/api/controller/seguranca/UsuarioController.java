package br.com.blank.api.controller.seguranca;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.blank.api.domain.seguranca.User;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    
    @Autowired
    public UsuarioService service;

    @ApiOperation("Retorna uma lista com todas as informações")
    @GetMapping()
    public List<UsuarioDTO> list(@RequestParam("page") int page, @RequestParam("size") int size) {
        List<UsuarioDTO> lista = this.service.findAll(page, size).stream().map(u -> new UsuarioDTO(u)).collect(Collectors.toList());
        return lista;
    }

    @ApiOperation("Retorna uma lista com todas as informações")
    @GetMapping("/pageable")
    public Page<UsuarioDTO> listPageable(@RequestParam("page") int page, @RequestParam("size") int size) {
        PageRequest pageable = PageRequest.of(page, size);
        Page<User> usariosPage = this.service.findAll(pageable);
        Page<UsuarioDTO> usuariosDtoPage = usariosPage.map(u -> new UsuarioDTO(u));
        return usuariosDtoPage;
    }

    @ApiOperation("Cria uma nova entidade")
    @PostMapping
    public UsuarioDTO create(@RequestBody @Valid UsuarioDTO entity) {
        return new UsuarioDTO(this.service.createOrUpdate(entity.getUsuario()));
    }

    @ApiOperation("Atualiza uma nova entidade")
    @PutMapping(value = "{id}")
    public UsuarioDTO update(@PathVariable(value = "id") long id, @RequestBody UsuarioDTO entity) {
        return new UsuarioDTO(this.service.createOrUpdate(entity.getUsuario()));
    }

    @ApiOperation("Deleta um novo registro")
    @DeleteMapping(value = "{id}")
    public void delete(@PathVariable(value = "id") long id) {
        this.service.delete(id);
    }

    @ApiOperation("Busca uma entidade pela sua identificação única")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Optional<UsuarioDTO> get(@PathVariable(value = "id") long id) {
        Optional<UsuarioDTO> usuarioDto = Optional.of(new UsuarioDTO(this.service.findById(id).get()));
        return usuarioDto;
    }
}
