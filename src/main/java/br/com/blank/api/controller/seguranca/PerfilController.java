package br.com.blank.api.controller.seguranca;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.blank.api.domain.seguranca.Perfil;
import br.com.blank.api.generics.GenericRestController;

@RestController
@RequestMapping("/perfis")
public class PerfilController extends GenericRestController<Perfil, PerfilService> {

}
