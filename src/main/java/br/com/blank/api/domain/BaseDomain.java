package br.com.blank.api.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@MappedSuperclass
public class BaseDomain implements Serializable {

	private static final long serialVersionUID = -1859115254387845325L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Long id;

	@Version
	@Column(name = "version")
	@JsonIgnoreProperties(allowGetters = false)
	private Long version;

	@Column(name = "ativo")
	private boolean ativo = true;

	public BaseDomain() {
	}

	public BaseDomain(Long id, Long version, boolean ativo) {
		super();
		this.id = id;
		this.version = version;
		this.ativo = ativo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
