package br.com.blank.api.domain.seguranca;

import br.com.blank.api.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @since 30/01/2022
 * @author Junior Lima
 */
@Entity
@Table(name = "perfil", schema = "seguranca")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Data
@EqualsAndHashCode(callSuper = false)
public class Perfil extends BaseDomain{

  private static final long serialVersionUID = 1L;

  @Column(name = "nome", nullable = false, length = 20)
  private String nome;

  @Column(name = "ativo", nullable = false)
  private boolean ativo;

  public Perfil(){
	this.ativo = true;
  }

}
