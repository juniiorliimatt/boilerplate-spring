package br.com.blank.api.domain.seguranca;

import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.blank.api.domain.BaseDomain;
import br.com.blank.api.domain.basico.ProfileEnum;

@Entity
@Table(name = "usuarios", schema = "seguranca")
public class User extends BaseDomain {

    private static final long serialVersionUID = 5852576210278434446L;
    private String email;
    private String nome;
    private String password;
    private ProfileEnum profile;
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ProfileEnum getProfile() {
        return profile;
    }

    public void setProfile(ProfileEnum profile) {
        this.profile = profile;
    }
}
