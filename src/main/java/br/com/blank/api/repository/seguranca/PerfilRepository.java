package br.com.blank.api.repository.seguranca;

import br.com.blank.api.domain.seguranca.Perfil;
import br.com.blank.api.repository.BaseRepository;

public interface PerfilRepository extends BaseRepository<Perfil> {

}
