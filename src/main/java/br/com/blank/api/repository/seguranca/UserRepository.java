package br.com.blank.api.repository.seguranca;

import br.com.blank.api.domain.seguranca.User;
import br.com.blank.api.repository.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

@Repository
public interface UserRepository extends BaseRepository<User> {
	User findByEmail(String email);
	
	@RequestMapping("/todos")
	Page<User> findAll(Pageable pageable);
}
