package br.com.blank.api.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import br.com.blank.api.domain.BaseDomain;

@NoRepositoryBean
public interface BaseRepository<T extends BaseDomain> extends JpaRepository<T, Serializable> {

}
