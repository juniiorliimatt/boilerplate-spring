package br.com.blank.api.repository.basico;

import br.com.blank.api.repository.BaseRepository;

public interface PessoaRepository extends BaseRepository<Pessoa> {}
