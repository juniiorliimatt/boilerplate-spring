package br.com.blank.api.utils;

import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils{

  private Utils(){
  }

  public static String formatDate(){
	LocalDateTime now = LocalDateTime.now();
	return now.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
  }

  public static String[] retornarCamposTab(String line) {
	Matcher matcher = Pattern.compile("\"[^\"]*\"|[^,]+").matcher(line);
	String linha = null;
	while(matcher.find()) {
	  if (linha == null) {
		linha = matcher.group();
	  } else {
		linha += matcher.group();
	  }
	}

	String[] array = linha.split("\t");

	return array;
  }

  public static String[] retornarCampos(String line) {
	Matcher matcher = Pattern.compile("\"[^\"]*\"|[^,]+").matcher(line);
	String linha = null;
	while(matcher.find()) {
	  if (linha == null) {
		linha = matcher.group();
	  } else {
		linha += "#!" + matcher.group();
	  }
	}

	String[] array = linha.split("#!");

	return array;
  }

  public static final Pattern DIACRITICS_AND_FRIENDS = Pattern.compile("[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+");

  public static String stripDiacritics(String str) {
	str = Normalizer.normalize(str, Normalizer.Form.NFD);
	str = DIACRITICS_AND_FRIENDS.matcher(str).replaceAll("");
	return str;
  }
}
