package br.com.blank.api.generics;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.blank.api.domain.BaseDomain;

public interface BaseService<T extends BaseDomain> {

    Optional<T> findById(Long id);

    List<T> findAll(int page, int count);

    Page<T> findAll(Pageable pageable);

    T createOrUpdate(T entity);

    void delete(Long id);

    long count();

}
