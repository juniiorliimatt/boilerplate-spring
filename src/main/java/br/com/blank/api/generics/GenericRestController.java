package br.com.blank.api.generics;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.blank.api.domain.BaseDomain;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.validation.Valid;

public class GenericRestController<T extends BaseDomain, S extends BaseService<T>> {

    @Autowired
    public S service;

    @ApiOperation("Retorna uma lista com todas as informações")
    @GetMapping()
    public List<T> list(@RequestParam("page") int page, @RequestParam("size") int size) {
        return this.service.findAll(page, size);
    }

    @ApiOperation("Retorna uma lista com todas as informações")
    @GetMapping("/pageable")
    public Page<T> listPageable(@RequestParam("page") int page, @RequestParam("size") int size) {
        PageRequest pageable = PageRequest.of(page, size);
        return this.service.findAll(pageable);
    }

    @ApiOperation("Cria uma nova entidade")
    @PostMapping
    public T create(@RequestBody @Valid T entity) {
        return this.service.createOrUpdate(entity);
    }

    @ApiOperation("Atualiza uma nova entidade")
    @PutMapping(value = "{id}")
    public T update(@PathVariable(value = "id") long id, @RequestBody T entity) {
        return this.service.createOrUpdate(entity);
    }

    @ApiOperation("Deleta um novo registro")
    @DeleteMapping(value = "{id}")
    public void delete(@PathVariable(value = "id") long id) {
        this.service.delete(id);
    }

    @ApiOperation("Busca uma entidade pela sua identificação única")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public Optional<T> get(@PathVariable(value = "id") long id) {
        return this.service.findById(id);
    }
}
