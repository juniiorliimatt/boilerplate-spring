package br.com.blank.api.generics;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import br.com.blank.api.domain.BaseDomain;
import br.com.blank.api.repository.BaseRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public class GenericServiceCrudImpl<T extends BaseDomain, D extends BaseRepository<T>> implements GenericServiceCrud<T, D> {

    @Autowired
    public D repository;

    public GenericServiceCrudImpl() {
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<T> findById(Long id) {
        return this.repository.findById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public List<T> findAll(int page, int size) {
        PageRequest pageable = PageRequest.of(page, size);
        return (List<T>) repository.findAll(pageable).getContent();
    }

    @Transactional
    @Override
    public T createOrUpdate(T entity) {
        return this.repository.save(entity);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        this.repository.deleteById(id);

    }

    @Transactional(readOnly = true)
    @Override
    public long count() {
        return this.repository.count();
    }

    @Override
    public Page<T> findAll(Pageable pageable) {
        return this.repository.findAll(pageable);
    }

}
