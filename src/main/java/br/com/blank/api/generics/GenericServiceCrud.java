package br.com.blank.api.generics;

import java.util.List;
import java.util.Optional;

import br.com.blank.api.domain.BaseDomain;
import br.com.blank.api.repository.BaseRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericServiceCrud<T extends BaseDomain, D extends BaseRepository<T>> extends BaseService<T> {

    Optional<T> findById(Long id);

    List<T> findAll(int page, int size);

    Page<T> findAll(Pageable pageable);

    T createOrUpdate(T entity);

    void delete(Long id);

    long count();
}
